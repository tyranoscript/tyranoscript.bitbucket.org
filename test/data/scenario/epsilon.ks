[_tb_system_call storage=system/_epsilon.ks]

*epsilon-start

[bg  time="100"  method="crossfade"  storage="stationfront-day.jpg"  ]
[playse  storage="3266.mp3"  ]
[cm  ]
#勇気
俺は駅の方に来ていた。電車までまだ時間あるし、そのへんでもブラブラするか。[lr]
.................。[lr]
駅の近くには、とあるゲームショップから電波ソングが流れていた。[lr]
ん、どこかで聞いたような曲だなあ。[lr]
そう思って店の方に振り向くと、そこには、二人の女の子がいた。[lr]


[cm  ]
[live2d_show  name="miku"  scale="1"  time="100"  left="97"  top="-13"  ]
[live2d_show  name="Epsilon"  scale="0.6"  time="100"  left="-173"  top="-132"  ]
[font  size="20"  color="0xff0000"  bold="true"  face="ＭＳ&nbsp;Ｐゴシック"  ]
[tb_image_show  time="1000"  storage="default/1434703325_bubbles.png"  width="128"  height="128"  x="125"  y="76"  _clickable_img=""  ]
#system
! "ツッコミの名手"が発動しました[lr]


[cm  ]
#system
! Authorの都合上、現在所有していないスキルが突然発動させられることがあります[lr]


[resetfont  ]
[tb_image_hide  time="500"  ]
[cm  ]
#響
なんでこのゲーム、登場人物が女の子ばかりなの?[lr]


[cm  ]
#Author
(それは、いい感じの2D男性モデルがないからです。誰か作って...。)[lr]


[cm  ]
#勇気
そういえば、右の子は知る人ぞ知る歌姫のボーカロイドだった。[lr]
どんな曲であろうと、依頼されれば歌うというのが、彼女たちの仕事だ。[lr]
ということは、左の子がプロデューサーであるPなのか。[lr]


[cm  ]
#勇気
といっても、この場合のPは、作詞、作曲、プロデュースなどを全部行う人のことを言うらしい。[lr]


[cm  ]
#勇気
見ていると、歌うのをやめた彼女たちがおしゃべりを始めた。[lr]
すると、ふとうちの学校の名前が出たので、俺はその会話に興味を持った。[lr]


[cm  ]
#ミク
「そういえば、絵里が通ってる学校に幽霊の噂があるって聞いたんだけど、[lr]
それは本当?」[lr]


[cm  ]
#絵里
「ん?何か悩みでもあるの?」[lr]


[cm  ]
#ミク
「うん、ちょっとね....」[lr]
ミクはそう言って口ごもった。[lr]


[cm  ]
#勇気
話が見えない...。[lr]
なぜ、うちの学校の幽霊(見たことないけど)が悩みと関係あるんだろう?[lr]
俺がそんなことを考えていると、その答えはすぐに出た。[lr]


[cm  ]
#ミク
「その幽霊って、どんな悩みでも解決してくれるって本当?」[lr]


[cm  ]
#絵里
「うーん、私も屋上に行ったことあるけど、特に何もなかったなあ」[lr]
「女の子がいただけで」[lr]


[cm  ]
#ミク
「じゃあ、その子が幽霊なんじゃ」[lr]


[cm  ]
#絵里
「そんなはずないよ。屋上にいたのは、普通の子だよ」[lr]
「学校の制服着てなかったのは、少し変だったけど」[lr]
「でも、幽霊って普通夜に出るでしょ」[lr]
「私、夜にたまたま学校の前を通りがかって...」[lr]


[cm  ]
#ミク
「えっ?それで、それで」[lr]
ミクは好奇心旺盛に続きを聞きたがった。そして、......実は、俺もだった。[lr]
早く続き、続き。[lr]


[cm  ]
#絵里
「でね...」[lr]


[cm  ]
#ミク、勇気
ごくり...[lr]


[cm  ]
#絵里
「...夜の屋上、人影が見えたけど...」[lr]
「...2人の人が喋ってただけだったよ」[lr]


[cm  ]
#ミク、勇気
がくり...[lr]
ミクと勇気から変な音がした。[lr]


[cm  ]
#絵里
「なんであんな時間にってちょっと思ったんだけどね」[lr]


[cm  ]
#ミク
「で、誰だったのか、分かる?」[lr]


[cm  ]
#絵里
「一人は影に隠れて見えなかったんだけど、もう一人は、響っていう人だった」[lr]


[cm  ]
#勇気
響が夜の屋上に人と会っていた。[lr]
一体、誰と会っていたんだろう。[lr]


[cm  ]
[font  size="20"  color="0xff0000"  bold="true"  face="ＭＳ&nbsp;Ｐゴシック"  ]
[tb_image_show  time="1000"  storage="default/1434973230_user-group.png"  width="128"  height="128"  x="122"  y="77"  _clickable_img=""  ]
#system
! "屋上の人影"を手に入れました[lr]


[tb_image_hide  time="500"  ]
[resetfont  ]
[stopse  ]
[live2d_hide  name="Epsilon"  time="1000"  ]
[live2d_hide  name="miku"  time="1000"  ]
[jump  storage="hibiki_2.ks"  target="*hibiki-2-start"  ]
[s  ]
