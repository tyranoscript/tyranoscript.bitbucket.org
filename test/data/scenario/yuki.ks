[_tb_system_call storage=system/_yuki.ks]

*yuki-start

[playse  storage="3257.mp3"  ]
[bg  time="100"  method="crossfade"  storage="corridor-day.jpg"  ]
#勇気
俺は、いつの間にか教室から廊下に出ていた。[lr]
そういえば、誰かと話していて、この学校で噂の「屋上に出る幽霊」の話をしていたような、[lr]
そんな気がする。[lr]
まあ、いいか。最近、どうも寝不足みたいだ。[lr]
俺は、気晴らしに外の空気を吸うため、屋上に行くことにした。[lr]


[cm  ]
[bg  time="100"  method="crossfade"  storage="stairwell-day.jpg"  ]
#勇気
屋上に行くために階段を登っていると、踊り場にクラスメイトの響がいる。[lr]
そういえば、最近、響は屋上によく行っているようで、なんとなく挙動不審に見える。[lr]
幽霊について、何か知っているのかもしれないな。[lr]
俺はそう考えて、特別興味があるわけでもない幽霊のことを響に聞いてみることにした。[lr]


[cm  ]
[live2d_show  name="hibiki"  scale="1"  time="1000"  left="276"  top="-240"  ]
[live2d_motion  name="hibiki"  filenm="idle_01.mtn"  idle="true"  ]
#勇気
「おーい、響」[lr]


[cm  ]
#響
「うん?」[lr]
「なに?私、忙しいんだけど」[lr]


[cm  ]
#勇気
相変わらず、響はなかなかにトゲトゲしいなあ。[lr]
しかし、ゲーム開始直後のこのタイミング、[lr]
せっかくだから、響でチュートリアルをはじめよう。[lr]


[live2d_hide  name="hibiki"  time="100"  ]
[stopse  ]
[jump  storage="hibiki.ks"  target="*hibiki-start"  ]
[s  ]
