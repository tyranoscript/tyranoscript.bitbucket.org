[_tb_system_call storage=system/_hibiki.ks]

*hibiki-start

[bg  time="100"  method="crossfade"  storage="stairwell-day.jpg"  ]
[playse  storage="3257.mp3"  loop="true"  clear="true"  ]
[live2d_show  name="hibiki"  scale="1"  time="100"  left="46"  top="-259"  ]
[live2d_motion  name="hibiki"  filenm="idle_01.mtn"  idle="true"  ]
[cm  ]
#勇気
「最近、屋上に行ってるみたいだけど、何かあるのか?」[lr]


[cm  ]
#響
「屋上には行ってない」[lr]


[cm  ]
[playse  storage="ストレートコントラスト.mp3"  loop="true"  clear="true"  ]
[font  size="20"  color="0xff0000"  bold="true"  face="ＭＳ&nbsp;Ｐゴシック"  ]
#system
! 能力が発動した[lr]


[resetfont  ]
*hibiki-battle-01-start

[cm  ]
#勇気
どうやら、響は嘘をついているようだ。[lr]
だとすると、どういうことになるだろうか?[lr]


[glink  color="white"  storage="hibiki.ks"  size="20"  text="屋上に何かある"  target="*hibiki-battle-01-end"  x="350"  y="150"  width="500"  ]
[glink  color="white"  storage="hibiki.ks"  size="20"  text="屋上には行ってない"  target="*hibiki-battle-01-no"  x="350"  y="200"  width="500"  ]
[s  ]
*hibiki-battle-01-no

[cm  ]
#勇気
どうやら、追求に失敗したみたいだ。[lr]
もう一度やってみよう。[lr]


[jump  storage="hibiki.ks"  target="*hibiki-battle-01-start"  ]
*hibiki-battle-01-end

[cm  ]
#勇気
どうやら、追求に成功したみたいだ。[lr]
響は、屋上に行っているようで、俺のような気晴らしが目的ではないらしい。[lr]
幽霊が昼間に出るなんてことは聞いたことがないので、それとは無関係だろうけど、[lr]
一体、何があるっていうんだ。[lr]


[cm  ]
#勇気
「で、屋上に何があるんだ?」[lr]


[cm  ]
#響
「はあ、そもそも屋上には行ってないって言ってるでしょ」[lr]
「ぶん殴るわよ。あんた...」[lr]


[cm  ]
#勇気
し、しまった。そういえば、響は否定してたっけ。[lr]
これについては、どうやら証拠を集めて、響にぶつけるしかないようだ。[lr]
ちなみに、響は本気で俺を殴ろうとしているだろうか?[lr]


*hibiki-battle-02-start

[cm  ]
[glink  color="white"  storage="hibiki.ks"  size="20"  text="彼女は本気だ"  x="350"  y="150"  width="500"  target="*hibiki-battle-02-end"  ]
[glink  color="white"  storage="hibiki.ks"  size="20"  text="本気で言ってるはずない"  x="350"  y="200"  width="500"  target="*hibiki-battle-02-no"  ]
[s  ]
[cm  ]
*hibiki-battle-02-no

#勇気
間違ってしまったようだ。[lr]
このゲーム、たまにギャグが入るらしいな...。[lr]
プレイヤーには少し厳しい仕様だ。[lr]


[cm  ]
#勇気
今後、間違えた場合、このような予告なく、[lr]
スタート画面や質問画面に戻ることがあるかもしれないが、驚かないで欲しい。[lr]
それは、ゲーム本来の仕様である。[lr]


[jump  storage="hibiki.ks"  target="*hibiki-battle-02-start"  ]
*hibiki-battle-02-end

#勇気
正解だ。[lr]
どうやら、彼女は半分本気で言ってるようだ。[lr]


[cm  ]
#勇気
今後、間違えた場合は予告なく、[lr]
スタート画面や質問画面に戻ることがあるかもしれないが、驚かないで欲しい。[lr]
それは、ゲーム本来の仕様である。[lr]


[cm  ]
#勇気
「あ、あははははは」[lr]
「じゃあ、俺はこれで。響、邪魔してごめん」[lr]


[cm  ]
#勇気
俺はすこしばかり身の危険を感じ、そこから離れることにした。[lr]
どうやら自分の目で確かめるしかないようだ。[lr]
屋上に行ってみよう。[lr]


[stopse  ]
[live2d_hide  name="hibiki"  time="100"  ]
[jump  storage="haru.ks"  target="*haru-start"  ]
[s  ]
