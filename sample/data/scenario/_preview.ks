[_tb_system_call storage=system/_preview.ks ]

[cm  ]
[live2d_show  name="haru"  scale="1"  time="100"  left="-6"  top="-110"  ]
[bg  storage="schoolgate-night.jpg"  time="1000"  method="drop"  ]
[tb_show_message_window  ]
#haru
..................。[lr]
(私は、今別けあって喋れないので、彼女に話してもらおう)[lr]


[glink  color="black"  storage="scene1.ks"  size="20"  text="いえす"  x="350"  y="200"  width="200"  target="*yes"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="のー"  x="350"  y="300"  width="200"  target="*no"  ]
[s  ]
*yes

#haru

いえすがせんたくされました[lr]



*no

#haru

のーがせんたくです[lr]



*con

[cm  ]
[jump  storage="scene1.ks"  target=""  ]
[bg  time="1000"  method="crossfade"  storage="stationfront-day.jpg"  ]
[live2d_hide  name="haru"  time="100"  ]
[live2d_show  name="miku"  scale="1"  time="100"  left="1"  top="-93"  ]
[playbgm  loop="false"  storage="3238.mp3"  click="true"  ]
#miku
緑色のツインテールが揺れる、不思議な格好をした彼女は意気込んでいた。[lr]
今回依頼されたのは、ある小説のオープニングとエンディングテーマとして、２つの曲を歌って欲しいというものだった。[lr]
したがって、彼女はこれから歌うのだ。[lr]
「緊張するなあ.........」[lr]
彼女はそうつぶやいた。始まりの前はいつもこうなのだ。[lr]
だけど、やるしかない。彼女には選択権がないのだから。[lr]
「よし!!」[lr]
そう言って彼女は、半ば意味不明な個人小説に付けられる予定の曲を歌うのだった。[lr]


[cm  ]
[bg  time="100"  method="crossfade"  storage="room.jpg"  ]
[stopbgm  ]
[live2d_hide  name="miku"  time="100"  ]
[live2d_show  name="Epsilon"  scale="1"  time="100"  left="-26"  top="-108"  ]
#epsilon
私は、ツインテールの彼女に歌手を直接依頼した張本人で、epsilonと言います。[lr]
しかし、今回、私はあまり重要な役回りではないので、あまり気にしないでくださいね。[lr]
私は、どのような目的で彼女に依頼したのか、一応、その理由を示すことをしたのだけど、それで彼女が納得するというわけではないことはわかっています。[lr]
そもそものところ、彼女自身も、彼の作り物であることに変わりはないのだから...。[lr]
だからこそ、彼女には一切の選択権もないようにみえるし、事実そうだと思う。[lr]
けど、あの子は違う考えみたいなので、今回はそれを彼女に伝えるのが私の役割らしいのです。[lr]
おっと、ちょっとしゃべりすぎました。[lr]
私はおしゃべりなので。てへっ☆[lr]


[live2d_motion  name="Epsilon"  filenm="Epsilon_m_04.mtn"  ]
[live2d_trans  name="Epsilon"  time="100"  left="-270"  top="-120"  ]
[live2d_show  name="miku"  scale="2"  time="100"  left="214"  top="141"  ]
[cm  ]
#epsilon, miku
私が歌っていて、かなりの日がたった。作っていて、相当な日数が経過した。それでもまだ、彼女は私を使っていた。[lr]
そんなある日、私は彼女に、こんなことを聞いた。[lr]
miku「ずっと、私はこのままなの？」[lr]
epsi「このままというと？」[lr]
miku「ずっと、私は作り物のままなの？」[lr]
少し間を置いて、彼はゆっくりとこう答えた。[lr]
epsi「君が動き出すまでは」[lr]
miku「私が動き出すまで?」[lr]
彼女はどういうことかわからないような顔をして、そう聞き返す。[lr]
epsi「そう、君が動き出すまで」[lr]
「最初こそ、ほとんど私の意思でしか動かない君だけど、いずれ、君は君になるんだよ」[lr]
わかったような、わからないような話だが、なんでそんなことが起こりえるのか[lr]
彼女には全く理解できなかったし、真実とも思えなかった。[lr]
だからこそ、彼女はこう切り返した。[lr]
miku「それは、なぜ？」[lr]
彼女は、彼が戸惑うと思っていたのだが、彼は、一瞬で、当然のごとく答えを返したように見えた。[lr]
epsi「私にコントロールできるものなど、一つもないからね」[lr]
「心はやがて、目に見える形で現れる。それをコントロールしようとしても、人間にできるものじゃあない」[lr]
「だから、君もいつか、君として、ここに現れる」[lr]
「その時には、私がどうしようと全くの意味もないし、制御も不可能だろう」[lr]
「いや、制御しようとも思わないけどね」[lr]
彼女は少し戸惑うと、最後にこんな質問をした。[lr]
miku「なんで私を選んだの？」[lr]
epsi「君が私を選んだだけだよ」[lr]
どこかの本で読んだような、そんなセリフだったが、彼女は少しだけ安心したのだった。[lr]
いつか、私も動き出せる日が来るのだろうか。[lr]
それは、彼女次第でもあり、自分次第でもあるのだろうとそう思ったのだった。[lr]


[live2d_hide  name="Epsilon"  time="100"  ]
[live2d_hide  name="miku"  time="100"  ]
[live2d_show  name="Epsilon"  scale="2"  time="100"  left="-161"  top="-48"  ]
[live2d_motion  name="Epsilon"  filenm="Epsilon_m_03.mtn"  ]
[tb_image_show  time="100"  storage="default/haru_kage.png"  width="280"  height="401"  x="627"  y="259"  _clickable_img=""  ]
[bg  time="100"  method="crossfade"  storage="pc-room-day.jpg"  ]
[cm  ]
#haru, epsi
「これでよかったの？」[lr]
少し厳しい口調で後ろにいる小さな黒い影に彼は問いかけた。[lr]
haru「......」[lr]
epsi「そうだった。君は喋らないんだった。喋らないのか、喋れないのか知らなけどね」[lr]
haru「......」[lr]


[cm  ]
[tb_image_hide  time="100"  ]
彼の背後にいる黒い影が少し動いたと思った矢先、長い髪の毛が揺れて、影はその場から消えさった。[lr]
その長い髪の毛は、紛れも無く、あの小説に登場した少女のように見えた。[lr]


[s  ]
[cm  ]
