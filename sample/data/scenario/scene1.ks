[_tb_system_call storage=system/_scene1.ks]

[cm  ]
[live2d_show  name="haru"  scale="1"  time="100"  left="-6"  top="-110"  ]
[bg  storage="schoolgate-night.jpg"  time="100"  method="drop"  ]
[tb_show_message_window  ]
.............[lr]


[cm  ]
[playbgm  loop="true"  storage="3238.mp3"  click="true"  ]
#haru
私は、haru。このゲームの司会役を努めさせていただきます。[lr]
まず、プレイヤーの考え、つまりあなたの考えを少しだけ教えてください。[lr]


[cm  ]
突然ですが、あなたは、今、何を考え、どのように感じていますか?[lr]


[glink  color="white"  storage="scene1.ks"  size="20"  text="今やることをやる。それだけだ"  target="*yes"  x="350"  y="300"  width="500"  ]
[glink  color="white"  storage="scene1.ks"  size="20"  text="自分の運命は自分で切り開いていく"  x="350"  y="200"  width="500"  target="*con"  ]
[glink  color="white"  storage="scene1.ks"  size="20"  text="昔はよかったなあ。あの頃見戻りたい"  x="350"  y="250"  width="500"  target="*no"  ]
[glink  color="white"  storage="scene1.ks"  size="20"  text="............."  target="*noun"  x="350"  y="350"  width="500"  ]
[s  ]
*no

#haru
あなたの考えは分かりました。[lr]
これからあなたに、一人の女性を紹介します。その女性と一緒に、一つの物語を進めてください。[lr]


[live2d_hide  name="haru"  time="100"  ]
[cm  ]
[bg  time="100"  method="crossfade"  storage="neighborhood-daytime.jpg"  ]
[live2d_show  name="Epsilon"  scale="1"  time="500"  left="-13"  top="-160"  ]
[playbgm  loop="false"  storage="3280.mp3"  click="true"  ]
#epsilon
はじめまして、私は、epsilon。[lr]
これからずっと一緒にいてくれるんだよね。嬉しいな~。[lr]
とりあえず、自己紹介も済んだことだし、この街をちょっとだけ案内するね。[lr]


[cm  ]
#あなた
俺は、あのことを少しでも話したかったのだけど.............[lr]
まあいいか。[lr]
そんなことを思い、街を案内してくれるという彼女についていくことにした。[lr]


[cm  ]
#author
"author":"syui",[lr]
"title":"epsilon",[lr]
"content":"ふわふわで、甘っぽい話にする"[lr]


[cm  ]
[live2d_hide  name="Epsilon"  time="100"  ]
[jump  storage="scene1.ks"  target=""  ]
[s  ]
*yes

#haru
あなたの考えは分かりました。[lr]
これからあなたに、一人の女性を紹介します。その女性と一緒に、一つの物語を進めてください。[lr]


[cm  ]
[live2d_hide  name="haru"  time="100"  ]
[bg  time="100"  method="crossfade"  storage="room.jpg"  ]
[live2d_show  name="hibiki"  scale="1"  time="500"  left="18"  top="-157"  ]
[live2d_motion  name="hibiki"  filenm="idle_01.mtn"  idle="true"  ]
[playbgm  loop="false"  storage="3257.mp3"  click="true"  ]
#hibiki
私はhibiki。[lr]
早速だけど、一つだけ君に言っておきたいとこがあるんだよね。[lr]
.............[lr]
私、君にはあまり興味ないから[lr]
だから、自分の好きな様にやらせてもらうので、そこんとこよろしく。[lr]
.............[lr]
といっても、このことを教室で話すのも何だし、とりあえず、屋上に行こうか。[lr]


[cm  ]
#あなた
.............[lr]
気の強そうな彼女と一緒に、俺は、あのことを解決していけるのだろうか......[lr]
それに、ほぼ初対面の相手に屋上に呼び出されるってどういうことだ.....[lr]
現実の学校では、屋上というのはロマンチックな場所でも何でもなく、[lr]
かつ、目の前の強気な彼女を見ていると、[lr]
脅迫されるか、ボコられるかの恐怖しかないのだが....[lr]
.............[lr]
しかし、呼び出しに応じないと、もっとひどいことになりそうだな。[lr]
俺は少し立ちすくんだあと、廊下に出て、屋上に向かうことにした。[lr]


[cm  ]
#author
"author":"syui",[lr]
"title":"hibiki",[lr]
"content":"ただただバカっぽくて面白い話にする"[lr]


[cm  ]
[live2d_hide  name="hibiki"  time="100"  ]
[jump  storage="scene1.ks"  target=""  ]
[s  ]
*con

#haru
あなたの考えは分かりました。[lr]
これからあなたに、一人の女性を紹介します。その女性と一緒に、一つの物語を進めてください。[lr]


[cm  ]
[live2d_hide  name="haru"  time="100"  ]
[bg  time="100"  method="crossfade"  storage="pc-room-day.jpg"  ]
[live2d_show  name="miku"  scale="1"  time="500"  left="-6"  top="-67"  ]
#miku
私は、miku。イニシャルは、Hatsune Mikuです。[lr]
君が相談に乗ってくれるってことでいいのかなあ。[lr]
よろしくね。[lr]


[cm  ]
#あなた
俺は、どう考えていいのか、分からなかった。[lr]
本当なら、彼女は悩んでいるはずなのだ。[lr]
しかし、彼女の平然とした振る舞いからは、[lr]
それが読み取れなかった、全然大丈夫...そんな風にすら感じさせた。[lr]
その時は、そんな気がしたんだ。[lr]


[cm  ]
#author
"author":"syui",[lr]
"title":"miku",[lr]
"content":"感情的な話にする。物語を直接的な場所から見る"[lr]


[live2d_hide  name="miku"  time="100"  ]
[jump  storage="scene1.ks"  target=""  ]
[s  ]
*noun

#haru
...................。[lr]
突然ですが、あなたは、今、何を考え、どのように感じていますか?[lr]


[glink  color="white"  storage="scene1.ks"  size="20"  text="今やることをやる。それだけだ"  target="*yes"  x="350"  y="300"  width="500"  ]
[glink  color="white"  storage="scene1.ks"  size="20"  text="自分の運命は自分で切り開いていく"  target="*con"  y="200"  width="500"  x="350"  ]
[glink  color="white"  storage="scene1.ks"  size="20"  target="*no"  text="昔はよかったなあ。あの頃見戻りたい"  x="350"  width="500"  y="250"  ]
[glink  color="white"  storage="scene1.ks"  size="20"  text="............."  target="*noun2"  width="500"  y="350"  x="350"  ]
[s  ]
*noun2

#haru
...................[lr]


[cm  ]
...................[lr]
私の選ぶのですか?[lr]
いいでしょう。すべての真実をお話した上で、私とともに、あの子の物語へ、いざ...。[lr]


[cm  ]
#あなた
予期せず初っ端から隠しリンクにアクセスしてしまったようだ...。[lr]
といっても、選択権も与えられないのはいかがなものかと思う。[lr]
多分、作者も分岐を用意するのが、面倒だったのだろう。[lr]
...............[lr]
しかし、俺は、少しばかりの恐怖心と好奇心が入り混じった気持ちで、[lr]
彼女と一緒に物語を見ていこう、そう思った。[lr]
...............[lr]


[cm  ]
#author
"author":"syui",[lr]
"title":"haru",[lr]
"content":"不思議な感じの話にする。物語を客観的な立場から見る"[lr]


[cm  ]
[live2d_hide  name="haru"  time="100"  ]
[jump  storage="scene1.ks"  target=""  ]
[s  ]
